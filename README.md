# WebGL Quadric Surfaces Visualizer #

This project consists in a simple quadric surfaces visualizer, which allows a user to create a 3D surface by introducing their respective parametric equations parameters. 
It also allows the user to rotate the 3D object automatically or manually (by dragging it with the mouse) and to zoom in/out with the mouse's scroll. 

## Main keywords ##

* Javascript
* WebGL
* Parametric equations
* 3D surfaces