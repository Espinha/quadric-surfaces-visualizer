//////////////////////////////////////////////////////////////////////////////
//
//  Animating models with global and local transformations.
//
//  References: www.learningwebgl.com + E. Angel examples
//
//  J. Madeira - October 2015
//
//////////////////////////////////////////////////////////////////////////////


//----------------------------------------------------------------------------
//
// Global Variables
//

var radius = 1.0;

var raxial = 2.0;

var height = 1.0;

var a = 0.0;

var b = 0.0;

var c = 0.0;

var factor = 5;

var gl = null; // WebGL context

var shaderProgram = null;

var triangleVertexPositionBuffer = null;

var triangleVertexColorBuffer = null;

var normals = [

		// FRONTAL TRIANGLE

		 0.0,  0.0,  1.0,

		 0.0,  0.0,  1.0,

		 0.0,  0.0,  1.0,
];

// The GLOBAL transformation parameters

var globalAngleYY = 0.0;

var globalTz = 0.0;

// The local transformation parameters

// The translation vector

var tx = 0.0;

var ty = 0.0;

var tz = 0.0;

// The rotation angles in degrees

var angleXX = 0.0;

var angleYY = 0.0;

var angleZZ = 0.0;

// The scaling factors

var sx = 0.5;

var sy = 0.5;

var sz = 0.5;

// NEW - GLOBAL Animation controls

var globalRotationYY_ON = 1;

var globalRotationYY_DIR = 1;

var globalRotationYY_SPEED = 1;

// NEW - Local Animation controls

var rotationXX_ON = 1;

var rotationXX_DIR = 1;

var rotationXX_SPEED = 1;

var rotationYY_ON = 1;

var rotationYY_DIR = 1;

var rotationYY_SPEED = 1;

var rotationZZ_ON = 1;

var rotationZZ_DIR = 1;

var rotationZZ_SPEED = 1;

// To allow choosing the way of drawing the model triangles

var enableMouse = 0;

var kAmbi = [ 0.2, 0.2, 0.2 ];

// Difuse coef.

var kDiff = [ 0.7, 0.7, 0.7 ];

// Specular coef.

var kSpec = [ 0.7, 0.7, 0.7 ];

// Phong coef.

var nPhong = 100;

// Initial model has just ONE TRIANGLE


var primitiveType = null;

// To allow choosing the projection type

var projectionType = 0;

// For storing the vertices defining the triangles

var vertices = [];

// And their colour

var colors = [

		 // FRONT FACE

		 1.00,  0.00,  0.00,

		 1.00,  0.00,  0.00,

		 1.00,  0.00,  0.00,


		 1.00,  1.00,  0.00,

		 1.00,  1.00,  0.00,

		 1.00,  1.00,  0.00,

		 // TOP FACE

		 0.00,  0.00,  0.00,

		 0.00,  0.00,  0.00,

		 0.00,  0.00,  0.00,


		 0.50,  0.50,  0.50,

		 0.50,  0.50,  0.50,

		 0.50,  0.50,  0.50,

		 // BOTTOM FACE

		 0.00,  1.00,  0.00,

		 0.00,  1.00,  0.00,

		 0.00,  1.00,  0.00,


		 0.00,  1.00,  1.00,

		 0.00,  1.00,  1.00,

		 0.00,  1.00,  1.00,

		 // LEFT FACE

		 0.00,  0.00,  1.00,

		 0.00,  0.00,  1.00,

		 0.00,  0.00,  1.00,


		 1.00,  0.00,  1.00,

		 1.00,  0.00,  1.00,

		 1.00,  0.00,  1.00,

		 // RIGHT FACE

		 0.25,  0.50,  0.50,

		 0.25,  0.50,  0.50,

		 0.25,  0.50,  0.50,


		 0.50,  0.25,  0.00,

		 0.50,  0.25,  0.00,

		 0.50,  0.25,  0.00,


		 // BACK FACE

		 0.25,  0.00,  0.75,

		 0.25,  0.00,  0.75,

		 0.25,  0.00,  0.75,


		 0.50,  0.35,  0.35,

		 0.50,  0.35,  0.35,

		 0.50,  0.35,  0.35,
];

//----------------------------------------------------------------------------
//
// The WebGL code
//

//----------------------------------------------------------------------------
//
//  Rendering
//

// Handling the Vertex and the Color Buffers

function initBuffers() {

	// Coordinates

	triangleVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
	triangleVertexPositionBuffer.itemSize = 3;
	triangleVertexPositionBuffer.numItems = vertices.length / 3;

	// Associating to the vertex shader

	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,
			triangleVertexPositionBuffer.itemSize,
			gl.FLOAT, false, 0, 0);

	// Colors

	triangleVertexColorBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexColorBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
	triangleVertexColorBuffer.itemSize = 3;
	triangleVertexColorBuffer.numItems = colors.length / 3;

	// Associating to the vertex shader

	gl.vertexAttribPointer(shaderProgram.vertexColorAttribute,
			triangleVertexColorBuffer.itemSize,
			gl.FLOAT, false, 0, 0);

}



function computeIllumination( mvMatrix ) {

	// Phong Illumination Model

	// Clearing the colors array

	for( var i = 0; i < colors.length; i++ )
	{
		colors[i] = 0.0;
	}

    // SMOOTH-SHADING

    // Compute the illumination for every vertex

    // Iterate through the vertices

    for( var vertIndex = 0; vertIndex < vertices.length; vertIndex += 3 )
    {
		// For every vertex

		// GET COORDINATES AND NORMAL VECTOR

		var auxP = vertices.slice( vertIndex, vertIndex + 3 );

		var auxN = normals.slice( vertIndex, vertIndex + 3 );

        // CONVERT TO HOMOGENEOUS COORDINATES

		auxP.push( 1.0 );

		auxN.push( 0.0 );

        // APPLY CURRENT TRANSFORMATION

        var pointP = multiplyPointByMatrix( mvMatrix, auxP );

        var vectorN = multiplyVectorByMatrix( mvMatrix, auxN );

        normalize( vectorN );

		// VIEWER POSITION

		var vectorV = vec3();

		if( projectionType == 0 ) {

			// Orthogonal

			vectorV[2] = 1.0;
		}
		else {

		    // Perspective

		    // Viewer at ( 0, 0 , 0 )

			vectorV = symmetric( pointP );
		}

        normalize( vectorV );

	    // Compute the 3 components: AMBIENT, DIFFUSE and SPECULAR

	    // FOR EACH LIGHT SOURCE

	    for(var l = 0; l < lightSources.length; l++ )
	    {
			if( lightSources[l].isOff() ) {

				continue;
			}

	        // INITIALIZE EACH COMPONENT, with the constant terms

		    var ambientTerm = vec3();

		    var diffuseTerm = vec3();

		    var specularTerm = vec3();

		    // For the current light source

		    ambient_Illumination = lightSources[l].getAmbIntensity();

		    int_Light_Source = lightSources[l].getIntensity();

		    pos_Light_Source = lightSources[l].getPosition();

		    // Animating the light source, if defined

		    var lightSourceMatrix = mat4();

		    // COMPLETE THE CODE FOR THE OTHER ROTATION AXES

		    if( lightSources[l].isRotYYOn() )
		    {
				lightSourceMatrix = mult(
						lightSourceMatrix,
						rotationYYMatrix( lightSources[l].getRotAngleYY() ) );
			}

	        for( var i = 0; i < 3; i++ )
	        {
			    // AMBIENT ILLUMINATION --- Constant for every vertex

			    ambientTerm[i] = ambient_Illumination[i] * kAmbi[i];

	            diffuseTerm[i] = int_Light_Source[i] * kDiff[i];

	            specularTerm[i] = int_Light_Source[i] * kSpec[i];
	        }

	        // DIFFUSE ILLUMINATION

	        var vectorL = vec4();

	        if( pos_Light_Source[3] == 0.0 )
	        {
	            // DIRECTIONAL Light Source

	            vectorL = multiplyVectorByMatrix(
							lightSourceMatrix,
							pos_Light_Source );
	        }
	        else
	        {
	            // POINT Light Source

	            // TO DO : apply the global transformation to the light source?

	            vectorL = multiplyPointByMatrix(
							lightSourceMatrix,
							pos_Light_Source );

				for( var i = 0; i < 3; i++ )
	            {
	                vectorL[ i ] -= pointP[ i ];
	            }
	        }

			// Back to Euclidean coordinates

			vectorL = vectorL.slice(0,3);

	        normalize( vectorL );

	        var cosNL = dotProduct( vectorN, vectorL );

	        if( cosNL < 0.0 )
	        {
				// No direct illumination !!

				cosNL = 0.0;
	        }

	        // SEPCULAR ILLUMINATION

	        var vectorH = add( vectorL, vectorV );

	        normalize( vectorH );

	        var cosNH = dotProduct( vectorN, vectorH );

			// No direct illumination or viewer not in the right direction

	        if( (cosNH < 0.0) || (cosNL <= 0.0) )
	        {
	            cosNH = 0.0;
	        }

	        // Compute the color values and store in the colors array

	        var tempR = ambientTerm[0] + diffuseTerm[0] * cosNL + specularTerm[0] * Math.pow(cosNH, nPhong);

	        var tempG = ambientTerm[1] + diffuseTerm[1] * cosNL + specularTerm[1] * Math.pow(cosNH, nPhong);

	        var tempB = ambientTerm[2] + diffuseTerm[2] * cosNL + specularTerm[2] * Math.pow(cosNH, nPhong);

			colors[vertIndex] += tempR;

	        // Avoid exceeding 1.0

			if( colors[vertIndex] > 1.0 ) {

				colors[vertIndex] = 1.0;
			}

	        // Avoid exceeding 1.0

			colors[vertIndex + 1] += tempG;

			if( colors[vertIndex + 1] > 1.0 ) {

				colors[vertIndex + 1] = 1.0;
			}

			colors[vertIndex + 2] += tempB;

	        // Avoid exceeding 1.0

			if( colors[vertIndex + 2] > 1.0 ) {

				colors[vertIndex + 2] = 1.0;
			}
	    }
	}
}


//----------------------------------------------------------------------------

//  Drawing the model

function drawModel( angleXX, angleYY, angleZZ,
					sx, sy, sz,
					tx, ty, tz,
					mvMatrix,
					primitiveType ) {

    // Pay attention to transformation order !!

	mvMatrix = mult( mvMatrix, translationMatrix( tx, ty, tz ) );

	mvMatrix = mult( mvMatrix, rotationZZMatrix( angleZZ ) );

	mvMatrix = mult( mvMatrix, rotationYYMatrix( angleYY ) );

	mvMatrix = mult( mvMatrix, rotationXXMatrix( angleXX ) );

	mvMatrix = mult( mvMatrix, scalingMatrix( sx, sy, sz ) );

	// Passing the Model View Matrix to apply the current transformation

	var mvUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");

	gl.uniformMatrix4fv(mvUniform, false, new Float32Array(flatten(mvMatrix)));

	// Drawing the contents of the vertex buffer

	// primitiveType allows drawing as filled triangles / wireframe / vertices

	computeIllumination( mvMatrix );

	initBuffers();

	if( primitiveType == gl.LINE_LOOP ) {


		var i;

		for( i = 0; i < triangleVertexPositionBuffer.numItems / 3; i++ ) {

			gl.drawArrays( primitiveType, 3 * i, 3 );
		}
	}
	else {

		gl.drawArrays(primitiveType, 0, triangleVertexPositionBuffer.numItems);

	}
}

//----------------------------------------------------------------------------

//  Drawing the 3D scene

function drawScene() {

	var pMatrix;

	var mvMatrix = mat4();

	// Clearing the frame-buffer and the depth-buffer

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// Computing the Projection Matrix

	if( projectionType == 0 ) {

		// For now, the default orthogonal view volume

		pMatrix = ortho( -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );

		// Global transformation !!

		globalTz = 0;

	}

	else {

		// A standard view volume.

		// Viewer is at (0,0,0)

		// Ensure that the model is "inside" the view volume

		pMatrix = perspective( 45, 1, 0.05, 15 );

		// Global transformation !!

		globalTz = -2.5;

		// TO BE DONE !

		// Allow the user to control the size of the view volume
	}

	// Passing the Projection Matrix to apply the current projection

	var pUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");

	gl.uniformMatrix4fv(pUniform, false, new Float32Array(flatten(pMatrix)));

	// GLOBAL TRANSFORMATION FOR THE WHOLE SCENE

	mvMatrix = translationMatrix( 0, 0, globalTz );

	// Instantianting the current model

	drawModel( angleXX, angleYY, angleZZ,
	           sx, sy, sz,
	           tx, ty, tz,
	           mvMatrix,
	           primitiveType );
}

//----------------------------------------------------------------------------
//
//  NEW --- Animation
//

// Animation --- Updating transformation parameters

var lastTime = 0;

function animate() {

	var timeNow = new Date().getTime();

	if( lastTime != 0 ) {

		var elapsed = timeNow - lastTime;

		// Global rotation

		if( globalRotationYY_ON ) {

			globalAngleYY += globalRotationYY_DIR * globalRotationYY_SPEED * (90 * elapsed) / 1000.0;
	    }

		// Local rotations

		if( rotationXX_ON ) {

			angleXX += rotationXX_DIR * rotationXX_SPEED * (90 * elapsed) / 1000.0;
	    }

		if( rotationYY_ON ) {

			angleYY += rotationYY_DIR * rotationYY_SPEED * (90 * elapsed) / 1000.0;
	    }

		if( rotationZZ_ON ) {

			angleZZ += rotationZZ_DIR * rotationZZ_SPEED * (90 * elapsed) / 1000.0;
	    }

	    if( lightSources[0].isRotYYOn() ) {

			var angle = lightSources[0].getRotAngleYY() + (90 * elapsed) / 1000.0;

			lightSources[0].setRotAngleYY( angle );
		}

		if( lightSources[1].isRotYYOn() ) {

			var angle = lightSources[1].getRotAngleYY() - 0.5 * (90 * elapsed) / 1000.0;

			lightSources[1].setRotAngleYY( angle );
		}
	}

	lastTime = timeNow;
}


//----------------------------------------------------------------------------

// Timer

function tick() {

	requestAnimFrame(tick);

	drawScene();

	animate();
}




//----------------------------------------------------------------------------
//
//  User Interaction
//

function outputInfos(){

}

//----------------------------------------------------------------------------

function setEventListeners(){

	 
 

		var canvas = document.getElementById("my-canvas");
		 var x;
		var y;
		var drag;
		  canvas.addEventListener("mousedown", function(event){
		    rotationXX_ON= 0;
		    rotationYY_ON = 0;
		    rotationZZ_ON = 0;
		    x = event.clientX;
		    y = event.clientY;
		 
		    drag = true;
		 
		    console.log("x = "+ x);
		    console.log("y = "+ y);
		    console.log("mouse pressed");
		 
		  },false);
		 
		  canvas.addEventListener("mousemove", function(event) {
		  //  rotationZZ_ON = 1;
		  //  rotationZZ_SPEED= 1.0;
		  var local_x = event.clientX;
		  var local_y = event.clientY;
		  var threshold = 3.0;
		  var d = Math.sqrt( (x-local_x)*(x-local_x) + (y-local_y)*(y-local_y) );
		  var d_x = x-local_x;
		  var d_y = y-local_y;
		 
		if(drag == true){
		  if(d_x > 0 && d_x > threshold){
		  //  console.log("drag right");
		    rotationXX_ON = 1;
		    rotationXX_DIR = 1;
		    rotationXX_SPEED=0.3;
		  }else if (d_x < 0 && d_x < -threshold) {
		 
		 
		  //  console.log("drag left");
		    rotationXX_ON = 1;
		    rotationXX_DIR = -1;
		    rotationXX_SPEED=0.3;
		  }
		 
		  if(d_y > 0 && d_y > threshold){
		 
		  //  console.log("drag up");
		    rotationYY_ON = 1;
		    rotationYY_DIR = -1;
		    rotationYY_SPEED=0.3;
		 
		  }
		  else if(d_y < 0 && d_y < -threshold ){
		    //console.log("drag down");
		    rotationYY_ON = 1;
		    rotationYY_DIR = 1;
		    rotationYY_SPEED=0.3;
		 
		  }
		}
		},false);
		 
		  canvas.addEventListener("mouseup", function(event){
		  rotationXX_ON= 0;
		  rotationYY_ON = 0;
		  drag = false;
		  console.log("mouse released");
		 
		},false);
	
	document.addEventListener("wheel", function(event) {
		if(enableMouse) {
			var zoom;
			console.log(event.wheelDelta);

			if(event.wheelDelta == 120) zoom = 0.1;
			else if(event.wheelDelta == -120) zoom = -0.1;

			console.log(zoom);

			console.log(zoom);
			sx += zoom;
			sy += zoom; 
			sz += zoom;
		}
	});
      

    // NEW --- Sphere approximation button

    document.getElementById("radius").onchange = function() {
    	radius = this.value;
    }

    document.getElementById("a").onchange = function() {
    	a = this.value;
    }

    document.getElementById("b").onchange = function() {
    	b = this.value;
    }

    document.getElementById("c").onchange = function() {
    	c = this.value;
    }

    document.getElementById("raxial").onchange = function() {
    	raxial = this.value;
    }

    document.getElementById("sphere-surf-button").onclick = function(){

    	primitiveType = gl.TRIANGLES;
        moveToSphericalSurface( vertices, radius );

        initBuffers();
        
	};

	document.getElementById("ellips").onclick = function(){

		primitiveType = gl.TRIANGLES;
        moveToEllipsoidSurface( vertices, a, b, c );

        initBuffers();

	};

	document.getElementById("torus").onclick = function(){

		primitiveType = gl.TRIANGLE_STRIP;

       	vertices = [];
       	normals = [];

       	var slices = 35;
       	var stacks = 35;

       	for(var theta = 0; theta < (2*Math.PI + 0.001); theta += (2*Math.PI + 0.001) / slices) {

       		for(var phi = 0; phi < (2*Math.PI + 0.001); phi += (2*Math.PI + 0.001) / stacks) {

       			var x = (parseInt(raxial) + radius * Math.cos(theta)) * Math.cos(phi);
       			var y = (parseInt(raxial) + radius * Math.cos(theta)) * Math.sin(phi);
       			var z = radius * Math.sin(theta);

       			normals.push(x);
       			normals.push(y);
       			normals.push(z);

       			vertices.push(x);
       			vertices.push(y);
       			vertices.push(z);

       			x = (parseInt(raxial) + radius * Math.cos(theta + 2*Math.PI / slices)) * Math.cos(phi);
       			y = (parseInt(raxial) + radius * Math.cos(theta + 2*Math.PI / slices)) * Math.sin(phi);
       			z = radius * Math.sin(theta + 2*Math.PI/slices);

       			normals.push(x);
       			normals.push(y);
       			normals.push(z);

       			vertices.push(x);
       			vertices.push(y);
       			vertices.push(z);
       		}
       	}


	    console.log(vertices);
	    computeVertexNormals(vertices, normals);
        initBuffers();
	};

	document.getElementById("cylinder").onclick = function() {

		primitiveType = gl.TRIANGLE_STRIP;

		vertices = [];	
		normals = [];

		var circleSlices = 30;
		var circleStep = (2*Math.PI + 0.001) / circleSlices;
		
		var heightSlices = 13;
		var heightStep = height/heightSlices;


		for(var i = 0; i < heightSlices; i++) {

			var z0 = height - i * heightStep;
			var z1 = z0 - heightStep;

			for(var j = 0; j <= circleSlices; j += circleStep) {
				
				var x = radius * Math.cos(j);
				var y = radius * Math.sin(j);

				vertices.push(x);
				vertices.push(y);
				vertices.push(z0);

				normals.push(x);
	        	normals.push(y);
	        	normals.push(z0);

	        	x = radius * Math.cos(j + circleStep);
				y = radius * Math.sin(j + circleStep);

				vertices.push(x);
				vertices.push(y);
				vertices.push(z1);

				normals.push(x);
	        	normals.push(y);
	        	normals.push(z1);
			}
		}

		computeVertexNormals(vertices, normals);
		initBuffers();


	}

	document.getElementById("cone").onclick = function() {


		primitiveType = gl.TRIANGLE_STRIP;

		vertices = [];
		normals = [];

		var circleSlices = 30;
		var circleStep = (2*Math.PI + 0.001) / circleSlices;
		
		var heightSlices = 13;
		var heightStep = height/heightSlices;

		for(var i = 0; i <= heightSlices; i++) {

			var z0 = 1 * height - i * heightStep;
			var z1 = z0 - heightStep;

			for(var j = 0; j <= circleSlices; j += circleStep) {
				
				var x = ((height-z0)/height) * radius * Math.cos(j);
				var y = ((height-z0)/height) * radius * Math.sin(j);

				vertices.push(x);
				vertices.push(y);
				vertices.push(z0);

				normals.push(x);
	        	normals.push(y);
	        	normals.push(z0);

	        	x = ((height-z1)/height) * radius * Math.cos(j + circleStep);
				y = ((height-z1)/height) * radius * Math.sin(j + circleStep);

				vertices.push(x);
				vertices.push(y);
				vertices.push(z1);

				normals.push(x);
	        	normals.push(y);
	        	normals.push(z1);
			}
		}

		computeVertexNormals(vertices, normals);
		initBuffers();

	}

	document.getElementById("paraboloid").onclick = function() {

		primitiveType = gl.TRIANGLE_STRIP;

		vertices = [];
		normals = [];


		var circleSlices = 30;
		var circleStep = (2*Math.PI + 0.001) / circleSlices;
		
		var heightSlices = 13;
		var heightStep = height/heightSlices;

		var z0, z1, x, y;
		for(var i = 0; i <= heightSlices; i++) {

			z0 = height - i * heightStep;
			z1 = z0 - heightStep;

			for(var j = 0; j <= circleSlices; j += circleStep) {
				
				x = Math.sqrt(z0/height) * radius * Math.cos(j);
				y = Math.sqrt(z0/height) * radius * Math.sin(j);

				vertices.push(x);
				vertices.push(y);
				vertices.push(z0);

				normals.push(x);
	        	normals.push(y);
	        	normals.push(z0);

	        	x = Math.sqrt(z1/height) * radius * Math.cos(j + circleStep);
				y = Math.sqrt(z1/height) * radius * Math.sin(j + circleStep);

				vertices.push(x);
				vertices.push(y);
				vertices.push(z1);

				normals.push(x);
	        	normals.push(y);
	        	normals.push(z1);
			}
		}

		computeVertexNormals(vertices, normals);
		initBuffers();
	}

	document.getElementById("surface1").onclick = function()  {
		
		primitiveType = gl.TRIANGLE_STRIP;

		vertices = [];
		normals = [];
		// z(x,y) = cos(x*5/180)*cos(y*PI/180) + sin(x*5/200)

		var levels = 10;

		var steps = levels / 10;
		var check = 1;
		var z;
		
		sx = 0.2;
		sy = 0.2;
		sz = 0.2;

		for(var i = -levels/2; i < levels/2; i+=steps) {
			
			if(check) {
				for(var j = -levels/2; j < levels/2; j+=steps) {
				
					vertices.push(i);
					vertices.push(j);
					z = Math.cos(i*factor)*Math.cos(j*factor) + Math.sin(i * factor);
					vertices.push(z);

					normals.push(i);
					normals.push(j);
					normals.push(z);

					vertices.push(i + steps);
					vertices.push(j);
					z = Math.cos((i+steps)*factor)*Math.cos(j*factor) + Math.sin((i+steps) * factor);
					vertices.push(z);

					normals.push(i + steps);
					normals.push(j);
					normals.push(z);

				}

				vertices.push(i)
				vertices.push(j - steps);
				z = Math.cos(i*factor)*Math.cos((j-steps)*factor) + Math.sin(i * factor);
				vertices.push(z);

				normals.push(i);
				normals.push(j - steps);
				normals.push(z);

				check = 0;
			}
			else {
				for(var j = levels/2; j >= -levels/2; j++) {
					vertices.push(i);
					vertices.push(j);
					z = Math.cos(i*factor)*Math.cos(j*factor) + Math.sin(i * factor);
					vertices.push(z);

					normals.push(i);
					normals.push(j);
					normals.push(z);

					vertices.push(i + steps);
					vertices.push(j);
					z = Math.cos((i+steps)*factor)*Math.cos(j*factor) + Math.sin((i+steps) * factor);
					vertices.push(z);

					normals.push(i + steps);
					normals.push(j);
					normals.push(z);
				}

				vertices.push(i);
				vertices.push(j + steps);
				z = Math.cos(i*factor)*Math.cos((j+steps)*factor) + Math.sin(i * factor);
				vertices.push(z);

				normals.push(i);
				normals.push(j + steps);
				normals.push(z);

			}

			check = 1;
		}

		computeVertexNormals(vertices, normals);
		initBuffers();
		
	}

	document.getElementById("surface2").onclick = function()  {
		
		vertices = [];
		normals = [];
		// z(x,y) = cos(x*5/180)*cos(y*PI/180) + sin(x*5/200)

		var levels = 10;

		var steps = levels / 10;
		var check = 1;
		var z;
		
		sx = 0.2;
		sy = 0.2;
		sz = 0.2;

		for(var i = -levels/2; i < levels/2; i+=steps) {
			
			if(check) {
				for(var j = -levels/2; j < levels/2; j+=steps) {
				
					vertices.push(i);
					vertices.push(j);
					z = factor * Math.cos(i) * Math.sin(j);
					vertices.push(z);

					normals.push(i);
					normals.push(j);
					normals.push(z);

					vertices.push(i + steps);
					vertices.push(j);
					z = factor * Math.cos(i+steps) * Math.sin(j);
					vertices.push(z);

					normals.push(i + steps);
					normals.push(j);
					normals.push(z);

				}

				vertices.push(i)
				vertices.push(j - steps);
				z = factor * Math.cos(i) * Math.sin(j-steps);
				vertices.push(z);

				normals.push(i);
				normals.push(j - steps);
				normals.push(z);

				check = 0;

			}
			else {
				for(var j = levels/2; j >= -levels/2; j++) {
					vertices.push(i);
					vertices.push(j);
					z = factor * Math.cos(i) * Math.sin(j);
					vertices.push(z);

					normals.push(i); 
					normals.push(j);
					normals.push(z);

					vertices.push(i + steps);
					vertices.push(j);
					z = factor * Math.cos(i + steps) * Math.sin(j);
					vertices.push(z);

					normals.push(i + steps);
					normals.push(j);
					normals.push(z);
				}

				vertices.push(i);
				vertices.push(j + steps);
				z = factor * Math.cos(i) * Math.sin(j + steps);
				vertices.push(z);

				normals.push(i);
				normals.push(j + steps);
				normals.push(z);

			}
			
			check = 1;
		}

			computeVertexNormals(vertices, normals);
			initBuffers();
		
	}

	document.getElementById("factor").onchange = function() {
		factor = this.value;
	}

	document.getElementById("radiusCyl").onchange = function() {
		radius = this.value;
	}

	document.getElementById("heightCyl").onchange = function() {
		height = this.value;
	}

	document.getElementById("factor2").onchange = function() {
		factor = this.value;
	}

	document.getElementById("heightP").onchange = function() {

		height = this.value;
	}

	document.getElementById("radiusP").onchange = function() {

		radius = this.value;
	}

	document.getElementById("radiusC").onchange = function() {

		radius = this.value;
	}

	document.getElementById("heightC").onchange = function() {

		height = this.value;
	}

	document.getElementById("radiusTorus").onchange = function() {

		radius = this.value;
	}


	// File loading

	// Adapted from:

	// http://stackoverflow.com/questions/23331546/how-to-use-javascript-to-read-local-text-file-and-read-line-by-line

	document.getElementById("file").onchange = function(){

		var file = this.files[0];

		var reader = new FileReader();

		reader.onload = function( progressEvent ){

			// Entire file read as a string

			// The tokens/values in the file

			// Separation between values is 1 or mode whitespaces

			var tokens = this.result.split(/\s\s*/);

			// Array of values; each value is a string

			var numVertices = parseInt( tokens[0] );

			// For every vertex we have 6 floating point values

			var i, j;

			var aux = 1;

			var newVertices = [];


			for( i = 0; i < numVertices; i++ ) {

				for( j = 0; j < 3; j++ ) {

					newVertices[ 3 * i + j ] = parseFloat( tokens[ aux++ ] );
				}

			}

			// Assigning to the current model

			vertices = newVertices.slice();

			//colors = newColors.slice();

			computeVertexNormals(vertices, normals);
			// Rendering the model just read

			initBuffers();

			// RESET the transformations - NEED AUXILIARY FUNCTION !!

			tx = ty = tz = 0.0;

			angleXX = angleYY = angleZZ = 0.0;

			sx = sy = sz = 0.5;
		};

		// Entire file read as a string

		reader.readAsText( file );
	}

    // Dropdown list

	var projection = document.getElementById("projection-selection");

	projection.addEventListener("click", function(){

		// Getting the selection

		var p = projection.selectedIndex;

		switch(p){

			case 0 : projectionType = 0;
				break;

			case 1 : projectionType = 1;
				break;
		}
	});

	// Dropdown list

	var list = document.getElementById("rendering-mode-selection");

	list.addEventListener("click", function(){

		// Getting the selection

		var mode = list.selectedIndex;

		switch(mode){

			case 0 : primitiveType = gl.TRIANGLE_STRIP;
				break;

			case 1 : primitiveType = gl.LINE_LOOP;
				break;

			case 2 : primitiveType = gl.POINTS;
				break;
		}
	});

	// Button events

	document.getElementById("XX-on-off-button").onclick = function(){

		// Switching on / off

		if( rotationXX_ON ) {

			rotationXX_ON = 0;
		}
		else {

			rotationXX_ON = 1;
		}
	};

	document.getElementById("XX-direction-button").onclick = function(){

		// Switching the direction

		if( rotationXX_DIR == 1 ) {

			rotationXX_DIR = -1;
		}
		else {

			rotationXX_DIR = 1;
		}
	};

	document.getElementById("XX-slower-button").onclick = function(){

		rotationXX_SPEED *= 0.75;
	};

	document.getElementById("XX-faster-button").onclick = function(){

		rotationXX_SPEED *= 1.25;
	};

	document.getElementById("YY-on-off-button").onclick = function(){

		// Switching on / off

		if( rotationYY_ON ) {

			rotationYY_ON = 0;
		}
		else {

			rotationYY_ON = 1;
		}
	};

	document.getElementById("YY-direction-button").onclick = function(){

		// Switching the direction

		if( rotationYY_DIR == 1 ) {

			rotationYY_DIR = -1;
		}
		else {

			rotationYY_DIR = 1;
		}
	};

	document.getElementById("YY-slower-button").onclick = function(){

		rotationYY_SPEED *= 0.75;
	};

	document.getElementById("YY-faster-button").onclick = function(){

		rotationYY_SPEED *= 1.25;
	};

	document.getElementById("ZZ-on-off-button").onclick = function(){

		// Switching on / off

		if( rotationZZ_ON ) {

			rotationZZ_ON = 0;
		}
		else {

			rotationZZ_ON = 1;
		}
	};

	document.getElementById("ZZ-direction-button").onclick = function(){

		// Switching the direction

		if( rotationZZ_DIR == 1 ) {

			rotationZZ_DIR = -1;
		}
		else {

			rotationZZ_DIR = 1;
		}
	};

	document.getElementById("ZZ-slower-button").onclick = function(){

		rotationZZ_SPEED *= 0.75;
	};

	document.getElementById("ZZ-faster-button").onclick = function(){

		rotationZZ_SPEED *= 1.25;
	};

	document.getElementById("reset-button").onclick = function(){

		// The initial values

		tx = 0.0;

		ty = 0.0;

		tz = 0.0;

		angleXX = 0.0;

		angleYY = 0.0;

		angleZZ = 0.0;

		sx = 0.5;

		sy = 0.5;

		sz = 0.5;

		rotationXX_ON = 0;

		rotationXX_DIR = 1;

		rotationXX_SPEED = 1;

		rotationYY_ON = 0;

		rotationYY_DIR = 1;

		rotationYY_SPEED = 1;

		rotationZZ_ON = 0;

		rotationZZ_DIR = 1;

		rotationZZ_SPEED = 1;

		enableMouse = 0;
	};

	document.getElementById("enable-disable-mouse").onclick = function() {

		if(!enableMouse) enableMouse = 1;
		else enableMouse = 0;
	}

	document.getElementById("face-culling-button").onclick = function(){

		if( gl.isEnabled( gl.CULL_FACE ) )
		{
			gl.disable( gl.CULL_FACE );
		}
		else
		{
			gl.enable( gl.CULL_FACE );
		}
	};

	document.getElementById("depth-test-button").onclick = function(){

		if( gl.isEnabled( gl.DEPTH_TEST ) )
		{
			gl.disable( gl.DEPTH_TEST );
		}
		else
		{
			gl.enable( gl.DEPTH_TEST );
		}
	};
}

//----------------------------------------------------------------------------
//
// WebGL Initialization
//

function initWebGL( canvas ) {
	try {

		// Create the WebGL context

		// Some browsers still need "experimental-webgl"

		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

		// DEFAULT: The viewport occupies the whole canvas

		// DEFAULT: The viewport background color is WHITE

		// NEW - Drawing the triangles defining the model

		primitiveType = gl.TRIANGLE_STRIP;

		// DEFAULT: Face culling is DISABLED

		// Enable FACE CULLING

		gl.disable( gl.CULL_FACE );

		// DEFAULT: The BACK FACE is culled!!

		// The next instruction is not needed...

		gl.cullFace( gl.BACK );

		gl.enable(gl.DEPTH_TEST);

	} catch (e) {
	}
	if (!gl) {
		alert("Could not initialise WebGL, sorry! :-(");
	}
}

//----------------------------------------------------------------------------

function runWebGL() {

	var canvas = document.getElementById("my-canvas");

	initWebGL( canvas );

	shaderProgram = initShaders( gl );

	setEventListeners();

	initBuffers();

	tick();		// NEW --- A timer controls the rendering / animation

	outputInfos();
}

